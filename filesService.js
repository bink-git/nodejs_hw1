// requires...
const fs = require('fs');
const path = require('path');

// constants...
const ext = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
const filesFolder = './files/';

const paramLog = (param, res) => {
  return res.status(400).json({ message: `Please specify ${param} parameter` });
};

function createFile(req, res, next) {
  // Your code to create the file.
  if (!req.body.filename) {
    return paramLog('filename', res);
  } else if (!req.body.content) {
    return paramLog('content', res);
  } else if (!ext.includes(path.extname(req.body.filename))) {
    return paramLog('valid file extension', res);
  }

  const filePath = path.join(__dirname, 'files', req.body.filename);

  if (fs.existsSync(filePath)) {
    return res.status(400).json({ message: `File already exists` });
  }

  const file = fs.writeFileSync(filePath, req.body.content);

  res.status(200).send({ message: 'File created successfully' });
}

function getFiles(req, res, next) {
  // Your code to get all files.
  const files = fs.readdirSync(path.join(__dirname, 'files'));

  res.status(200).send({
    message: 'Success',
    files,
  });
}

const getFile = (req, res, next) => {
  // Your code to get all files.
  let filename = req.params.filename;
  const filePath = filesFolder + filename;

  if (!fs.existsSync(filePath)) {
    next({
      message: 'No file',
      status: 400,
    });
    return;
  }

  const extension = filename.split('.').pop();
  const content = fs.readFileSync(filePath, {
    encoding: 'utf8',
    flag: 'r',
  });
  const { birthtime: uploadedDate } = fs.statSync(filePath);

  return res.status(200).send({
    message: 'Success',
    filename,
    content,
    extension,
    uploadedDate,
  });
};

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
};
